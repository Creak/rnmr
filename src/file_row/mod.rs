mod imp;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::Object;
use gtk::glib;

use crate::file_object::FileObject;

glib::wrapper! {
    pub struct FileRow(ObjectSubclass<imp::FileRow>)
    @extends gtk::Box, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl Default for FileRow {
    fn default() -> Self {
        Self::new()
    }
}

impl FileRow {
    pub fn new() -> Self {
        Object::builder().build()
    }

    pub fn bind(&self, file_object: &FileObject) {
        // Get state
        let path_label = self.imp().path_label.get();
        let mut bindings = self.imp().bindings.borrow_mut();

        // Bind `file_object.content` to `file_row.path_label.label`
        let content_label_binding = file_object
            .bind_property("path", &path_label, "label")
            .sync_create()
            .build();
        // Save binding
        bindings.push(content_label_binding);
    }

    pub fn unbind(&self) {
        // Unbind all stored bindings
        for binding in self.imp().bindings.borrow_mut().drain(..) {
            binding.unbind();
        }
    }
}
