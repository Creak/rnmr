use std::cell::RefCell;

use adw::subclass::prelude::*;
use glib::Binding;
use gtk::glib;

// Object holding the state
#[derive(Default, gtk::CompositeTemplate)]
#[template(resource = "/com/foolstep/Rnmr/file_row.ui")]
pub struct FileRow {
    #[template_child]
    pub path_label: TemplateChild<gtk::Label>,
    // Vector holding the bindings to properties of `FileObject`
    pub bindings: RefCell<Vec<Binding>>,
}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for FileRow {
    // `NAME` needs to match `class` attribute of template
    const NAME: &'static str = "RnmrFileRow";
    type Type = super::FileRow;
    type ParentType = gtk::Box;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

// Trait shared by all GObjects
impl ObjectImpl for FileRow {}

// Trait shared by all widgets
impl WidgetImpl for FileRow {}

// Trait shared by all boxes
impl BoxImpl for FileRow {}
