use std::cell::RefCell;

use adw::subclass::prelude::*;
use glib::subclass::InitializingObject;
use gtk::{gio, glib};

// Object holding the state
#[derive(gtk::CompositeTemplate, Default)]
#[template(resource = "/com/foolstep/Rnmr/window.ui")]
pub struct RnmrWindow {
    #[template_child]
    pub files_list: TemplateChild<gtk::ListView>,
    pub files: RefCell<Option<gio::ListStore>>,
}

#[gtk::template_callbacks]
impl RnmrWindow {
    #[template_callback]
    fn handle_button_clicked(&self, _: &gtk::Button) {
        self.obj().open_files();
    }
}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for RnmrWindow {
    // `NAME` needs to match `class` attribute of template
    const NAME: &'static str = "RnmrWindow";
    type Type = super::RnmrWindow;
    type ParentType = gtk::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
        klass.bind_template_callbacks();

        // Create action to open files and add to action group "win"
        klass.install_action("win.open-files", None, |window, _, _| {
            window.open_files();
        });
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

// Trait shared by all GObjects
impl ObjectImpl for RnmrWindow {
    fn constructed(&self) {
        // Call "constructed" on parent
        self.parent_constructed();

        // Setup
        let obj = self.obj();
        obj.setup_files();
        obj.setup_factory();
    }
}

// Trait shared by all widgets
impl WidgetImpl for RnmrWindow {}

// Trait shared by all windows
impl WindowImpl for RnmrWindow {}

// Trait shared by all application windows
impl ApplicationWindowImpl for RnmrWindow {}
