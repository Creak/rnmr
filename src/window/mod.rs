mod imp;

use std::path::PathBuf;

use adw::prelude::*;
use glib::Object;
use gtk::glib::subclass::types::ObjectSubclassIsExt;
use gtk::{gio, glib};

use crate::file_object::FileObject;
use crate::file_row::FileRow;

glib::wrapper! {
    pub struct RnmrWindow(ObjectSubclass<imp::RnmrWindow>)
        @extends gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl RnmrWindow {
    pub fn new(app: &adw::Application) -> Self {
        // Create new window
        Object::builder().property("application", app).build()
    }

    fn open_files(&self) {
        let dialog = gtk::FileDialog::builder().build();

        let win = self.clone();
        dialog.open_multiple(Some(self), gio::Cancellable::NONE, move |result| {
            if let Ok(models) = result {
                let paths = RnmrWindow::get_files(&models)
                    .into_iter()
                    .filter_map(|f| f.path())
                    .collect::<Vec<PathBuf>>();

                for path in paths {
                    win.new_file(&path);
                }
            }
        });
    }

    fn get_files(models: &gio::ListModel) -> Vec<gio::File> {
        models
            .snapshot()
            .into_iter()
            .filter_map(|obj| obj.downcast::<gio::File>().ok())
            .collect::<Vec<_>>()
    }

    fn files(&self) -> gio::ListStore {
        // Get state
        self.imp()
            .files
            .borrow()
            .clone()
            .expect("Could not get current files.")
    }


    fn setup_files(&self) {
        // Create new model
        let model = gio::ListStore::new::<FileObject>();

        // Get state and set model
        self.imp().files.replace(Some(model));

        // Wrap model with selection and pass it to the list view
        let selection_model = gtk::NoSelection::new(Some(self.files()));
        self.imp().files_list.set_model(Some(&selection_model));
    }

    fn setup_factory(&self) {
        // Create a new factory
        let factory = gtk::SignalListItemFactory::new();

        // Create an empty `FileRow` during setup
        factory.connect_setup(move |_, list_item| {
            // Create `FileRow`
            let file_row = FileRow::new();
            list_item
                .downcast_ref::<gtk::ListItem>()
                .expect("Needs to be ListItem")
                .set_child(Some(&file_row));
        });

        // Tell factory how to bind `FileRow` to a `FileObject`
        factory.connect_bind(move |_, list_item| {
            // Get `FileObject` from `ListItem`
            let file_object = list_item
                .downcast_ref::<gtk::ListItem>()
                .expect("Needs to be ListItem")
                .item()
                .and_downcast::<FileObject>()
                .expect("The item has to be an `FileObject`.");

            // Get `FileRow` from `ListItem`
            let file_row = list_item
                .downcast_ref::<gtk::ListItem>()
                .expect("Needs to be ListItem")
                .child()
                .and_downcast::<FileRow>()
                .expect("The child has to be a `FileRow`.");

            file_row.bind(&file_object);
        });

        // Tell factory how to unbind `FileRow` from `FileObject`
        factory.connect_unbind(move |_, list_item| {
            // Get `FileRow` from `ListItem`
            let file_row = list_item
                .downcast_ref::<gtk::ListItem>()
                .expect("Needs to be ListItem")
                .child()
                .and_downcast::<FileRow>()
                .expect("The child has to be a `FileRow`.");

            file_row.unbind();
        });

        // Set the factory of the list view
        self.imp().files_list.set_factory(Some(&factory));
    }

    fn new_file(&self, path: &PathBuf) {
        // Add new file to model
        let file = FileObject::new(path);
        self.files().append(&file);
    }}
