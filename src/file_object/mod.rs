mod imp;

use std::path::PathBuf;

use glib::Object;
use gtk::glib;

glib::wrapper! {
    pub struct FileObject(ObjectSubclass<imp::FileObject>);
}

impl FileObject {
    pub fn new(path: &PathBuf) -> Self {
        Object::builder()
            .property("path", path)
            .build()
    }
}

#[derive(Default)]
pub struct FileData {
    pub path: PathBuf,
}
