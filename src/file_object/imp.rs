use std::cell::RefCell;
use std::path::PathBuf;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::Properties;
use gtk::glib;

use super::FileData;

// Object holding the state
#[derive(Properties, Default)]
#[properties(wrapper_type = super::FileObject)]
pub struct FileObject {
    #[property(name = "path", get, set, type = PathBuf, member = path)]
    pub data: RefCell<FileData>,
}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for FileObject {
    const NAME: &'static str = "RnmrFileObject";
    type Type = super::FileObject;
}

// Trait shared by all GObjects
#[glib::derived_properties]
impl ObjectImpl for FileObject {}
