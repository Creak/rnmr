mod window;
mod file_object;
mod file_row;

use adw::prelude::*;
use gtk::{gio, glib};
use window::RnmrWindow;

const APP_ID: &str = "com.foolstep.Rnmr";

fn main() -> glib::ExitCode {
    gio::resources_register_include!("rnmr.gresource").expect("Failed to register resources.");

    // Create a new application
    let app = adw::Application::builder().application_id(APP_ID).build();

    // Connect to signals
    app.connect_startup(setup_shortcuts);
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn setup_shortcuts(app: &adw::Application) {
    app.set_accels_for_action("win.open-files", &["<Ctrl>o"]);
}

fn build_ui(app: &adw::Application) {
    // Create a new custom window and present it
    let window = RnmrWindow::new(app);
    window.present();
}
